package Assignment_2_Task_2;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static java.lang.Thread.sleep;


public class OnlineShoppingClient {
    private boolean mainPage = false;
    private boolean account = false;
    private boolean cart = false;
    private boolean checkout = false;



    WebDriver browser;
    public OnlineShoppingClient(){
        System.setProperty("webdriver.chrome.driver", "src/test/chromedriver.exe");
        this.browser = new ChromeDriver();

    }

     boolean VisitMainPage() throws InterruptedException {
        if(!(mainPage || account || cart || checkout)){
            mainPage = true;
            browser.get("https://www.agendamalta.com/#");
            return true;
        }else if(cart || account){
            browser.findElement(By.xpath("//*[@id=\"global-header\"]/div[1]")).click();
            sleep(2*100);
            account = false;
            cart = false;
            mainPage = true;
            return true;
        }else if(mainPage){
            browser.findElement(By.id("ctl00_AgendaSearch_txtTopSearch")).sendKeys("Book Of Pf");
            browser.findElement(By.id("ctl00_AgendaSearch_btnTopSearch")).click();
            sleep(2*500);
            browser.findElement(By.xpath("//*[@id=\"products\"]/div[1]/div[5]")).click();
            sleep(2*100);
            mainPage = true;
            return true;
        }else {
            throw new IllegalStateException();
        }

    }

    boolean account() throws InterruptedException {
        if(cart  || mainPage){
            sleep(2*100);
            if(browser.findElement(By.xpath("//*[@id=\"global-header\"]/div[2]/div[1]/div[2]/ul/li[1]")).getText().equals("Register")){
                browser.findElement(By.xpath("//li[@class='login']")).click();
                sleep(2*100);
                browser.findElement(By.xpath("//input[@id='ctl00_Login_txtUsername']")).sendKeys("dancam0099");
                browser.findElement(By.xpath("//input[@id='ctl00_Login_txtPassword']")).sendKeys("R8Wzh");
                browser.findElement(By.xpath("//input[@id='ctl00_Login_btnLogin']")).click();
            }else{
                browser.findElement(By.xpath("//*[@id=\"global-header\"]/div[2]/div[1]/div[2]/ul/li[3]")).click();
            }
            account = true;
            mainPage = false;
            cart = false;

            return true;
        }else if( checkout){
            if(browser.findElement(By.xpath("//*[@id=\"aspnetForm\"]/div[3]/section[1]/div/section")).getText().equals("Cart\n" +
                    "There are no items in your cart.")){

                if(browser.findElement(By.xpath("//*[@id=\"global-header\"]/div[2]/div[1]/div[2]/ul/li[1]")).getText().equals("Account")){
                    browser.findElement(By.className("cart")).click();
                    checkout = false;
                    account = true;
                    return true;
                }

                browser.findElement(By.xpath("//li[@class='login']")).click();
                sleep(2*100);
                browser.findElement(By.xpath("//input[@id='ctl00_Login_txtUsername']")).sendKeys("dancam0099");
                browser.findElement(By.xpath("//input[@id='ctl00_Login_txtPassword']")).sendKeys("R8Wzh");
                browser.findElement(By.xpath("//input[@id='ctl00_Login_btnLogin']")).click();
                checkout = false;
                account = true;
                return true;
            }
            if(browser.findElement(By.xpath("//*[@id=\"global-header\"]/div[2]/div[1]/div[2]/ul/li[1]")).getText().equals("Register")){
                browser.findElement(By.xpath("//*[@id=\"ctl00_placeholderBody_txtloginUsername\"]']")).sendKeys("dancam0099");
                browser.findElement(By.xpath("//*[@id=\"ctl00_placeholderBody_txtPassword\"]]")).sendKeys("R8Wzh");
                browser.findElement(By.className("cart")).click();
            }
            checkout = false;
            account = true;
            return true;
        }else {
            this.VisitMainPage();
            account = true;
            return true;
        }
    }

    boolean cart() throws InterruptedException {
        sleep(2*500);
        if(mainPage || account){
            browser.findElement(By.className("cart")).click();
            mainPage = false;
            account = false;
            cart = true;
            return true;
        }else if(cart){
            sleep(2*100);
            if(!browser.findElement(By.xpath("//*[@id=\"aspnetForm\"]/div[3]/section[1]/div/section")).getText().equals("Cart\n" +
                    "There are no items in your cart.")){
                browser.findElement(By.xpath("//*[@id=\"ctl00_placeholderBody_lvCart_ctrl0_lbtnDelete\"]/i")).click();
            }
            cart = true;
            return true;
        }else {
            this.VisitMainPage();
            browser.findElement(By.className("cart")).click();
            cart = true;
            return true;
        }
    }

    boolean checkOut() throws InterruptedException {
        if(cart){
            try{
                sleep(2*100);
                if(!browser.findElement(By.xpath("//*[@id=\"aspnetForm\"]/div[3]/section[1]/div/section")).getText().equals("Cart\n" +
                        "There are no items in your cart.")){
                    browser.findElement(By.xpath("//*[@id=\"ctl00_placeholderBody_lvCart_btnCheckout\"]")).click();
                }

            }catch (NoSuchElementException e){
                System.out.println();;
            }

            checkout = true;
            account = false;
            cart = false;
            return true;
        }else{
            throw new IllegalStateException();
        }
    }

    boolean isCheckout(){return checkout;}
    boolean isMainPage(){return mainPage;}
    boolean isCart(){return cart;}
    boolean isAccount(){return account;}














}
