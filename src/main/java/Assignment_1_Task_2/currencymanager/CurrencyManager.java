package Assignment_1_Task_2.currencymanager;

import Assignment_1_Task_2.currencymanager.currencies.Currency;
import Assignment_1_Task_2.currencymanager.currencies.CurrencyDatabase;
import Assignment_1_Task_2.currencymanager.currencies.ExchangeRate;

import Assignment_1_Task_2.currencymanager.currencyserver.DefaultCurrencyServer;


import java.util.List;
import java.util.Scanner;

public class CurrencyManager {

    static CurrencyDatabase currencyDatabase;

    public static void main(String[] args) throws Exception {
        currencyDatabase = new CurrencyDatabase();
        Scanner sc = new Scanner(System.in);
        boolean exit = false;

        while (!exit) {
            System.out.println("\nMain Menu\n---------\n");

            System.out.println("1. List currencies");
            System.out.println("2. List exchange rates between major currencies");
            System.out.println("3. Check exchange rate");
            System.out.println("4. Add currency");
            System.out.println("5. Delete currency");
            System.out.println("0. Quit");

            System.out.print("\nEnter your choice: ");

            int choice = sc.nextInt();

            switch (choice) {
                case 0:
                    exit = true;
                    break;
                case 1:
                    List<Currency> currencies = currencyDatabase.getCurrencies();
                    System.out.println("\nAvailable Currencies\n--------------------\n");
                    for (Currency currency : currencies) {
                        System.out.println(currency.toString());
                    }
                    break;
                case 2:
                    List<ExchangeRate> exchangeRates = currencyDatabase.getMajorCurrencyRates();
                    System.out.println("\nMajor Currency Exchange Rates\n-----------------------------");
                    for (ExchangeRate rate : exchangeRates) {
                        System.out.println(rate.toString());
                    }
                    break;
                case 3:
                    System.out.println("\nEnter source currency code (e.g. EUR): ");
                    String src = sc.next().toUpperCase();
                    System.out.println("\nEnter destination currency code (e.g. GBP): ");
                    String dst = sc.next().toUpperCase();
                    try {
                        ExchangeRate rate = currencyDatabase.getExchangeRate(src, dst, new DefaultCurrencyServer());
                        System.out.println(rate.toString());
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 4:
                    System.out.println("\nEnter the currency code: ");
                    String code = sc.next().toUpperCase();
                    System.out.println("Enter currency name: ");
                    String name = sc.next();
                    name += sc.nextLine();

                    String major = "\n";
                    while (!(major.equalsIgnoreCase("y") || major.equalsIgnoreCase("n"))) {
                        System.out.println("Is this a major currency? [y/n]");
                        major = sc.next();
                    }

                    try {
                        currencyDatabase.addCurrency(new Currency(code, name, major.equalsIgnoreCase("y")));
                    } catch (Exception e) {
                        System.err.println(e.getMessage());
                    }
                    break;
                case 5:
                    System.out.println("\nEnter the currency code: ");
                    code = sc.next().toUpperCase();
                    try {
                        currencyDatabase.deleteCurrency(code);
                    } catch (Exception e) {
                        System.err.println(e.getMessage());
                    }
                    break;
            }

            Thread.sleep(1000);
        }
    }
}
