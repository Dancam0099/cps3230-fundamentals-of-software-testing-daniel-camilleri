package Assignment_1_Task_2.currencymanager;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class CurrencyManagerTest {
    CurrencyManager currencyManager;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;


    @Before
    public void setup() throws Exception {
        currencyManager = new CurrencyManager();
        System.setOut(new PrintStream(outContent));
        System.out.flush();
    }

    @Test
    public void mainMainMenuTestTest() throws Exception {
        String data = "0\n"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertEquals("Main Menu",output[1]);
    }

    @Test
    public void mainPrintCurrenciesTest() throws Exception{
        String data = "1\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertEquals("GBP - British Pound\r",output[17]);
    }

    @Test
    public void mainListExchangeRatesTest() throws Exception{
        String data = "2\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertEquals("Major Currency Exchange Rates",output[12]);
    }

    @Test
    public void mainGetExchangeRateSuccessfulTest()throws Exception{
        String data = "3\nEUR\nGBP\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        String[] line = output[15].split(" ");
        Assert.assertEquals("EUR",line[0]);
    }

    @Test
    public void mainGetExchangeRateExceptionTest() throws Exception{
        String data = "3\nTST\nTST\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n");
        Assert.assertEquals("Unkown currency: TST\r",output[15]);
    }

    @Test
    public void mainAddCurrencySuccessfulTest() throws Exception {
        String data = "4\nTST\nTEST\ny\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        Assert.assertTrue(CurrencyManager.currencyDatabase.currencyExists("TST"));
        CurrencyManager.currencyDatabase.deleteCurrency("TST");
    }

    @Test
    public void mainAddCurrencyAlreadyExistsTest() throws Exception{
        String data = "4\nEUR\nEuro\ny\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        System.setErr(new PrintStream(outContent));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertEquals("The currency EUR already exists.\r",output[15]);
    }

    @Test
    public void mainRemoveCurrencyCorrectTest() throws Exception{
        String data = "4\nTST\nTest\ny\n5\nTST\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        Assert.assertFalse(CurrencyManager.currencyDatabase.currencyExists("TST"));
    }

    @Test
    public void mainRemoveCurrencyException() throws Exception{
        String data = "5\nTST\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        System.setErr(new PrintStream(outContent));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertNotEquals("Enter the currency code: Currency does not exist: TST\n",output[12]);
    }



    @After
    public void cleanup(){
        currencyManager = null;
        System.setOut(originalOut);
        System.setErr(originalErr);
        System.out.flush();
        System.err.flush();
    }


}
