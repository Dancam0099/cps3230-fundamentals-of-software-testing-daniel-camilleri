package Assignment_1_Task_2.currencymanager.stubs;

import Assignment_1_Task_2.currencymanager.currencyserver.CurrencyServer;



public class CorrectDefaultCurrencyServer implements CurrencyServer {
    public double getExchangeRate(String sourceCurrency, String destinationCurrency) {
        return 0.5;
    }
}
