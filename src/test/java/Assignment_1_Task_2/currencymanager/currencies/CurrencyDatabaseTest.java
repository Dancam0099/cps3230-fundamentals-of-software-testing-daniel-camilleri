package Assignment_1_Task_2.currencymanager.currencies;
import Assignment_1_Task_2.currencymanager.currencyserver.DefaultCurrencyServer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.PrintStream;
import java.util.List;



public class CurrencyDatabaseTest {

    CurrencyDatabase currencyDatabase;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalErr = System.err;
    @Before
    public void setup(){
        try {
            currencyDatabase = new CurrencyDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void currencyExistsTrueTest(){

        Assert.assertTrue(currencyDatabase.currencyExists("EUR"));
    }


    @Test
    public void currencyExistsFalseTest(){
        Assert.assertFalse(currencyDatabase.currencyExists("CBD"));
    }

    @Test
    public void getCurrenciesListSizeReturnTest(){
        Assert.assertNotEquals(0,currencyDatabase.getCurrencies().size());
    }

    @Test
    public void getMajorCurrenciesListSizeReturnTest(){
        Assert.assertNotEquals(0,currencyDatabase.getMajorCurrencies().size());
    }

    @Test
    public void getCurrencyByCodeAvailableCurrencyTest(){
        Assert.assertEquals("EUR",currencyDatabase.getCurrencyByCode("EUR").code);
    }

    @Test
    public void getCurrencyByCodeNotAvailableCurrencyTest(){
        Assert.assertNull(currencyDatabase.getCurrencyByCode("TST"));
    }

    @Test
    public void addCurrencySuccessfulTest() throws Exception {
        Currency currency = new Currency("TST","Test",true);
        currencyDatabase.addCurrency(currency);
        Assert.assertEquals("TST",currencyDatabase.getCurrencyByCode("TST").code);
        currencyDatabase.deleteCurrency("TST");
    }


    @Test
    public void addCurrencyExceptionNullTest(){
        try{
            currencyDatabase.addCurrency(null);
        }catch (Exception e){
            Assert.assertEquals("ERROR: cannot add currency",e.getMessage());
        }
    }

    @Test
    public void addCurrencyExceptionIncorrectCodeLengthTest(){
        try{
            Currency currency = new Currency("T","Test",true);
            currencyDatabase.addCurrency(currency);
        }catch (Exception e){
            Assert.assertEquals("A currency code should have 3 characters.",e.getMessage());
        }
    }

    @Test
    public void addCurrencyExceptionIncorrectNameLengthTest(){
        try{
            Currency currency = new Currency("TST","T",true);
            currencyDatabase.addCurrency(currency);
        }catch (Exception e){
            Assert.assertEquals("A currency's name should be at least 4 characters long.",e.getMessage());
        }
    }

    @Test
    public void addCurrencyExceptionCurrencyAlreadyExistsTest(){
        try{
            Currency currency = new Currency("EUR","EURO",true);
            currencyDatabase.addCurrency(currency);
        }catch (Exception e){
            Assert.assertEquals("The currency EUR already exists.",e.getMessage());
        }
    }


    @Test
    public void deleteCurrencySuccessfulTest() throws Exception{
        Currency currency = new Currency("TST","Test",true);
        currencyDatabase.addCurrency(currency);
        currencyDatabase.deleteCurrency("TST");
        Assert.assertFalse(currencyDatabase.currencyExists("TST"));
    }

    @Test
    public void deleteCurrencyExceptionNullTest(){
        try{
            currencyDatabase.deleteCurrency(null);
        }catch (Exception e){
            Assert.assertEquals("Error: Invalid code",e.getMessage());
        }
    }

    @Test
    public void deleteCurrencyExceptionCurrencyDoesntExistsTest(){
        try{
            currencyDatabase.deleteCurrency("TST");
        }catch (Exception e){
            Assert.assertEquals("Currency does not exist: TST",e.getMessage());
        }
    }


    @Test (expected = Exception.class)
    public void getExchangeRateSourceCurrencyNullTest() throws Exception {
        Assert.assertNull(currencyDatabase.getExchangeRate(null, "EUR", new DefaultCurrencyServer()));
    }
    @Test (expected = Exception.class)
    public void getExchangeRateDestinationCurrencyNullTest() throws Exception {
        Assert.assertNull(currencyDatabase.getExchangeRate("USD",null,new DefaultCurrencyServer()));
    }

    @Test
    public void getExchangeCurrencyNotInDatabaseTest() throws Exception{
        Currency currency = new Currency("TST","Test",true);
        currencyDatabase.addCurrency(currency);
        ExchangeRate exchangeRateSource = currencyDatabase.getExchangeRate("USD","EUR",new DefaultCurrencyServer());
        ExchangeRate exchangeRateDestination = currencyDatabase.exchangeRates.get("USDEUR");
        Assert.assertEquals(exchangeRateSource.destinationCurrency,exchangeRateDestination.destinationCurrency);
        currencyDatabase.deleteCurrency("TST");
    }
    @Test
    public void getExchangeCurrencyLongerThanFiveMinutesTest() throws Exception {
        Currency currency = new Currency("TST", "Test", true);
        currencyDatabase.addCurrency(currency);
        ExchangeRate exchangeRateSourceOld = currencyDatabase.getExchangeRate("USD", "EUR", new DefaultCurrencyServer());
        currencyDatabase.exchangeRates.get("USDEUR").timeLastChecked = 0;
        ExchangeRate exchangeRateSourceNew = currencyDatabase.getExchangeRate("USD", "EUR", new DefaultCurrencyServer());
        System.out.println();

        Assert.assertNotEquals(exchangeRateSourceNew.rate, exchangeRateSourceOld.rate);
        currencyDatabase.deleteCurrency("TST");
    }


    @Test
    public void getMajorCurrencyRates() throws Exception{
        List<ExchangeRate> exchangeRates = currencyDatabase.getMajorCurrencyRates();
        //3 currencies with 3 rates therefore /2 to get only currencies.
        Assert.assertEquals(3,exchangeRates.size()/2);
    }

    @Test
    public void initFirstLineIncorrect() throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(currencyDatabase.currenciesFile));
        writer.write("test\n");
        writer.flush();
        writer.close();
        try{
            currencyDatabase.init();
        }catch (Exception e){
            Assert.assertEquals("Parsing error when reading currencies file.",e.getMessage());
        }
        currencyDatabase.persist();

    }

    @Test
    public void initMoreThanTwoCommasTest() throws Exception{
        BufferedWriter writer = new BufferedWriter(new FileWriter(currencyDatabase.currenciesFile));
        writer.write("code,name,major\n");
        writer.write(",,,,\n");
        writer.flush();
        writer.close();
        try{
            currencyDatabase.init();
        }catch (Exception e){
            Assert.assertEquals("Parsing error: expected two commas in line ,,,,",e.getMessage());
        }
        currencyDatabase.persist();

    }

    @Test
    public void initTooLongCurrencyCodeTest() throws Exception {
        Currency currency = new Currency("TSTTT","Test",true);
        System.setErr(new PrintStream(outContent));
        currencyDatabase.init();
        currencyDatabase.currencies.add(currency);
        currencyDatabase.persist();
        currencyDatabase.init();
        String[] output = outContent.toString().split("\n");

        Assert.assertEquals("Invalid currency code detected: TSTTT\r",output[0]);

        currencyDatabase.currencies.remove(currency);
        currencyDatabase.persist();
    }


    @After
    public void cleanup(){
        currencyDatabase = null;
        System.setErr(originalErr);
        System.err.flush();
    }





}
