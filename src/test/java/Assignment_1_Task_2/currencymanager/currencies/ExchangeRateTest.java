package Assignment_1_Task_2.currencymanager.currencies;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ExchangeRateTest {
    ExchangeRate exchangeRate;

    @Before
    public void setup(){
        Currency currencySource = new Currency("TST","Test",true);
        Currency currencyDestination = new Currency("DFD","Dffd",true);
        exchangeRate = new ExchangeRate(currencySource,currencyDestination,5.4);
    }

    @Test
    public void checkSourceCurrencyTest(){
        Currency currency = exchangeRate.sourceCurrency;
        Assert.assertEquals("TST",currency.code);
    }

    @Test
    public void checkDestinationCurrencyTest(){
        Currency currency = exchangeRate.destinationCurrency;
        Assert.assertEquals("DFD",currency.code);
    }


    @Test
    public void checkRate(){
        Assert.assertEquals( 5.4,exchangeRate.rate,0.001);

    }



    @Test
    public void toStringTest(){
        Assert.assertEquals("TST 1 = DFD 5.40",exchangeRate.toString());
    }



    @After
    public void cleanup(){
        exchangeRate = null;
    }


}
