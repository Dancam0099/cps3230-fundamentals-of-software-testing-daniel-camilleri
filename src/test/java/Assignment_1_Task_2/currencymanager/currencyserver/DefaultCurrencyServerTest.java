package Assignment_1_Task_2.currencymanager.currencyserver;

import Assignment_1_Task_1.currencymanager.currencyserver.CurrencyServer;
import Assignment_1_Task_1.currencymanager.currencyserver.DefaultCurrencyServer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DefaultCurrencyServerTest {
    CurrencyServer defaultCurrencyServer;

    @Before
    public void setup(){
        defaultCurrencyServer = new DefaultCurrencyServer();

    }

    @Test
    public void getExcangeTest(){
        Assert.assertNotEquals(0,defaultCurrencyServer.getExchangeRate("TDT", "TST"), 0.0);
    }



    @After
    public void cleanup(){
        defaultCurrencyServer = null;
    }


}
