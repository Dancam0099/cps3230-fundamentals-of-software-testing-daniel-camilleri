package Assignment_1_Task_2.currencymanager.currencyserver;
import Assignment_1_Task_2.currencymanager.currencies.CurrencyDatabase;
import Assignment_1_Task_2.currencymanager.currencies.ExchangeRate;
import Assignment_1_Task_2.currencymanager.stubs.CorrectDefaultCurrencyServer;
import Assignment_1_Task_2.currencymanager.stubs.IncorrectDefaultCurrencyServer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;



import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CurrencyServerTest {

    CurrencyServer currencyServer;
    CurrencyDatabase  currencyDatabase;

    @Before
    public void setup() throws Exception {

        currencyServer = mock(CurrencyServer.class);
        currencyDatabase = new CurrencyDatabase();
    }

    @Test
    public void getExchangeRateMockedTest() throws Exception {
        when(currencyServer.getExchangeRate(Matchers.anyString(),Matchers.anyString())).thenReturn(0.5);
        ExchangeRate exchangeRate =  currencyDatabase.getExchangeRate("EUR","USD",currencyServer);
        Assert.assertEquals(0.5,exchangeRate.rate,0.01);
    }

    @Test
    public void getExchangeRateCorrectStubTest() throws Exception{
        currencyServer = new CorrectDefaultCurrencyServer();
        ExchangeRate exchangeRate =  currencyDatabase.getExchangeRate("EUR","USD",currencyServer);
        Assert.assertEquals(1,exchangeRate.rate,0.01);
    }

    @Test
    public void getExchangeRateIncorrectStubTest() throws Exception{
        currencyServer = new IncorrectDefaultCurrencyServer();
        try{
            currencyDatabase.getExchangeRate("EUR","USD",currencyServer);
        }catch (Exception e){
            Assert.assertEquals("Internal Error contact system admin.",e.getMessage());
        }


    }




    @After
    public void cleanup(){
        currencyServer = null;
    }


}
