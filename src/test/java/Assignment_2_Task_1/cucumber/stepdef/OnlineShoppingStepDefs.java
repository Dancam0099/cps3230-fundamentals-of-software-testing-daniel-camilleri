package Assignment_2_Task_1.cucumber.stepdef;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

import static java.lang.Thread.sleep;
import static org.junit.Assert.*;



public class OnlineShoppingStepDefs {
    WebDriver browser;
    // https://www.agendamalta.com/#

    @Given("I am a user on  the website")
    public void iAmAUserOnTheWebsite() {
        browser.get("https://www.agendamalta.com/#");
    }

    @When("I log in using valid credentials")
    public void i_log_in(){
        browser.findElement(By.xpath("//li[@class='login']")).click();
        browser.findElement(By.xpath("//input[@id='ctl00_Login_txtUsername']")).sendKeys("dancam0099");
        browser.findElement(By.xpath("//input[@id='ctl00_Login_txtPassword']")).sendKeys("R8Wzh");
        browser.findElement(By.xpath("//input[@id='ctl00_Login_btnLogin']")).click();
    }

    @Then("I should be logged in")
    public void iShouldBeLoggedIn() {
        assertEquals("Account",browser.findElement(By.xpath("//li[@class='register']")).getText());
    }

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", "src/test/chromedriver.exe");
        browser = new ChromeDriver();
    }

    @After
    public void teardown() {
        browser.quit();
    }


    @When("I log in using invalid credentials")
    public void iLogInUsingInvalidCredentials() {
        browser.findElement(By.xpath("//li[@class='login']")).click();
        browser.findElement(By.xpath("//input[@id='ctl00_Login_txtUsername']")).sendKeys("test123");
        browser.findElement(By.xpath("//input[@id='ctl00_Login_txtPassword']")).sendKeys("R8Wzh");
        browser.findElement(By.xpath("//input[@id='ctl00_Login_btnLogin']")).click();
    }


    @Then("I should not be logged in")
    public void iShouldNotBeLoggedIn() {
        assertEquals("Username and password do not match",browser.findElement(By.id("ctl00_Login_vsErrorSummary")).getText());
    }

    @Given("I am a user on the website")
    public void iAmAUserOnTheWebsite2() {
        browser.get("https://www.agendamalta.com/#");
    }

    @Given("I am a logged in user on the website")
    public void iAmALoggedInUserOnTheWebsite() {
        browser.get("https://www.agendamalta.com/#");
        browser.findElement(By.xpath("//li[@class='login']")).click();
        browser.findElement(By.xpath("//input[@id='ctl00_Login_txtUsername']")).sendKeys("dancam0099");
        browser.findElement(By.xpath("//input[@id='ctl00_Login_txtPassword']")).sendKeys("R8Wzh");
        browser.findElement(By.xpath("//input[@id='ctl00_Login_btnLogin']")).click();
    }

    @When("I search for a product")
    public void iSearchForAProduct() throws InterruptedException {
        browser.findElement(By.id("ctl00_AgendaSearch_txtTopSearch")).sendKeys("Book Of Pf");
        browser.findElement(By.id("ctl00_AgendaSearch_btnTopSearch")).click();
        sleep(2*100);
    }

    @And("I select the first product in the list")
    public void iSelectTheFirstProductInTheList() throws InterruptedException {
        browser.findElement(By.xpath("//*[@id=\"products\"]/div[1]/h3")).click();
        sleep(2*500);

    }

    @Then("I should see the product details")
    public void iShouldSeeTheProductDetails() {
        assertEquals("Book Of Pf",browser.findElement(By.xpath("//*[@id=\"ctl00_placeholderBody_lblTitle\"]")).getText());
    }

    @And("my shopping cart is empty")
    public void myShoppingCartIsEmpty() throws InterruptedException {
        browser.findElement(By.xpath("//*[@id=\"global-header\"]/div[2]/div[1]/div[2]/ul/li[2]/a")).click();
        sleep(2*100);
        assertEquals("There are no items in your cart.",browser.findElement(By.xpath("//*[@id=\"aspnetForm\"]/div[3]/section[1]/div/section/p")).getText());
        sleep(2*100);
        browser.findElement(By.xpath("//*[@id=\"global-header\"]/div[1]")).click();
        sleep(2*100);
        browser.findElement(By.xpath("//*[@id=\"tabs\"]/ul/li[2]/a")).click();
        sleep(2*500);
    }

    @When("I view the details of a product")
    public void iViewTheDetailsOfAProduct() throws InterruptedException {
        browser.findElement(By.xpath("//*[@id=\"products2\"]/div[1]")).click();
        sleep(2*100);


    }

    @And("I choose to buy the product")
    public void iChooseToBuyTheProduct() throws InterruptedException {
        browser.findElement(By.xpath("//*[@id=\"ctl00_placeholderBody_AddToCart\"]/span[2]")).click();
        sleep(2*100);
    }

    @Then("my shopping cart should contain {int} item")
    public void myShoppingCartShouldContainItem(int cartsize) throws InterruptedException {
        browser.findElement(By.xpath("//*[@id=\"global-header\"]/div[2]/div[1]/div[2]/ul/li[2]/a")).click();
        sleep(2*100);
        List<WebElement> shoppinglist = browser.findElements(By.xpath("//*[@id=\"aspnetForm\"]/div[3]/section[1]/div/section/table/tbody/tr"));
        assertEquals(cartsize,shoppinglist.size());
    }


    @When("I add {int} products to my shopping cart")
    public void iAddNumProductsProductsToMyShoppingCart(int product_num) throws InterruptedException {
        List<WebElement> shoppinglist = browser.findElements(By.xpath("//div[@id=\"products2\"]/div"));
        if(product_num == 10)
            product_num -=2;

        for (int i = 0; i < product_num; i++) {
            WebElement temp = shoppinglist.get(i);
            temp.findElement(By.className("icon-basket")).click();
        }

        if(product_num == 8){
            browser.findElement(By.xpath("//*[@id=\"tabs-2\"]/div[3]/div/a[1]")).click();
            sleep(2*500);
            shoppinglist = browser.findElements(By.xpath("//div[@id=\"products2\"]/div"));
            for (int i = 0; i < 2; i++) {
                WebElement temp = shoppinglist.get(i);
                temp.findElement(By.className("icon-basket")).click();
            }

        }
        sleep(2*100);
    }

    @Then("my shopping cart should contain {int} items")
    public void myShoppingCartShouldContainNumProductsItems(int product_num) throws InterruptedException {
        browser.findElement(By.xpath("//*[@id=\"global-header\"]/div[2]/div[1]/div[2]/ul/li[2]/a")).click();
        sleep(2*100);
        List<WebElement> shoppinglist = browser.findElements(By.xpath("//*[@id=\"aspnetForm\"]/div[3]/section[1]/div/section/table/tbody/tr"));
        assertEquals(product_num,shoppinglist.size());
        System.out.println();

    }

    @And("my shopping cart has {int} products")
    public void myShoppingCartHasProducts(int arg0) throws InterruptedException {
        browser.findElement(By.xpath("//*[@id=\"tabs\"]/ul/li[2]/a")).click();
        sleep(2*500);
        List<WebElement> shoppinglist = browser.findElements(By.xpath("//div[@id=\"products2\"]/div"));
        for (int i = 0; i < 2; i++) {
            WebElement temp = shoppinglist.get(i);
            temp.findElement(By.className("icon-basket")).click();
        }
        browser.findElement(By.xpath("//*[@id=\"global-header\"]/div[2]/div[1]/div[2]/ul/li[2]/a")).click();
        sleep(2*100);
        System.out.println();
    }

    @When("I remove the first product in my cart")
    public void iRemoveTheFirstProductInMyCart() throws InterruptedException {
        browser.findElement(By.xpath("//*[@id=\"ctl00_placeholderBody_lvCart_ctrl0_lbtnDelete\"]/i")).click();
        sleep(2*100);
    }
}
