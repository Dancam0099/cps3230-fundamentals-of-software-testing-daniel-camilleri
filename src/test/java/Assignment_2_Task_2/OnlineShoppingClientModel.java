package Assignment_2_Task_2;


import Assignment_2_Task_2.enums.OnlineShoppingStates;
import nz.ac.waikato.modeljunit.Action;
import nz.ac.waikato.modeljunit.FsmModel;
import nz.ac.waikato.modeljunit.GreedyTester;
import nz.ac.waikato.modeljunit.StopOnFailureListener;
import nz.ac.waikato.modeljunit.Tester;
import nz.ac.waikato.modeljunit.coverage.ActionCoverage;
import nz.ac.waikato.modeljunit.coverage.StateCoverage;
import nz.ac.waikato.modeljunit.coverage.TransitionPairCoverage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Random;

public class OnlineShoppingClientModel implements FsmModel{

    private OnlineShoppingClient systemUnderTest = new OnlineShoppingClient();
    private OnlineShoppingStates modelOS = OnlineShoppingStates.MAINPAGE;
    private boolean mainPage = false;
    private boolean account = false;
    private boolean cart = false;
    private boolean checkout = false;


    public OnlineShoppingStates getState() {
        return modelOS;
    }

    public void reset(boolean b) {
        if(b){
            systemUnderTest = new OnlineShoppingClient();
        }
        modelOS = OnlineShoppingStates.MAINPAGE;
        mainPage = false;
        account = false;
        cart = false;
        checkout = false;

    }

    public boolean mainPageGuard() {return getState().equals(OnlineShoppingStates.MAINPAGE) || getState().equals(OnlineShoppingStates.ACCOUNT)
                                        || getState().equals(OnlineShoppingStates.CART);}
    public @Action void mainPage() throws InterruptedException {
        systemUnderTest.VisitMainPage();
        modelOS = OnlineShoppingStates.MAINPAGE;
        mainPage = true;
        account = false;
        cart = false;
        checkout = false;
        Assert.assertTrue(systemUnderTest.isMainPage());
    }

    public boolean accountGuard(){return getState().equals(OnlineShoppingStates.MAINPAGE) || getState().equals(OnlineShoppingStates.CART)
                                      || getState().equals(OnlineShoppingStates.CHECKOUT);}
    public @Action void account() throws InterruptedException {
        systemUnderTest.account();
        modelOS = OnlineShoppingStates.ACCOUNT;
        mainPage = false;
        account = true;
        cart = false;
        checkout = false;
        Assert.assertTrue(systemUnderTest.isAccount());
    }

    public boolean cartGuard(){ return getState().equals(OnlineShoppingStates.MAINPAGE) || getState().equals(OnlineShoppingStates.CART)
                                    || getState().equals(OnlineShoppingStates.ACCOUNT); }
    public @Action void cart() throws InterruptedException {
        systemUnderTest.cart();
        modelOS = OnlineShoppingStates.CART;
        mainPage = false;
        account = false;
        cart = true;
        checkout = false;
        Assert.assertTrue(systemUnderTest.isCart());

    }

    public boolean checkoutGuard(){ return getState().equals(OnlineShoppingStates.CART);}
    public @Action void checkout() throws InterruptedException {
        systemUnderTest.checkOut();
        modelOS = OnlineShoppingStates.CHECKOUT;
        mainPage = false;
        account = false;
        cart = false;
        checkout = true;
        Assert.assertTrue(systemUnderTest.isCheckout());
    }


    @Test
    public void OnlineShoppingClientOperator() {
        //System.setProperty("webdriver.chrome.driver", "src/test/chromedriver.exe");
        final GreedyTester tester = new GreedyTester(new OnlineShoppingClientModel()); //Creates a test generator that can generate random walks. A greedy random walk gives preference to transitions that have never been taken before. Once all transitions out of a state have been taken, it behaves the same as a random walk.
        tester.setRandom(new Random()); //Allows for a random path each time the model is run.
        tester.buildGraph(); //Builds a model of our FSM to ensure that the coverage metrics are correct.
        tester.addListener(new StopOnFailureListener()); //This listener forces the test class to stop running as soon as a failure is encountered in the model.
        tester.addListener("verbose"); //This gives you printed statements of the transitions being performed along with the source and destination states.
        tester.addCoverageMetric(new TransitionPairCoverage()); //Records the transition pair coverage i.e. the number of paired transitions traversed during the execution of the test.
        tester.addCoverageMetric(new StateCoverage()); //Records the state coverage i.e. the number of states which have been visited during the execution of the test.
        tester.addCoverageMetric(new ActionCoverage()); //Records the number of @Action methods which have ben executed during the execution of the test.
        tester.generate(100); //Generates 500 transitions
        tester.printCoverage(); //Prints the coverage metrics specified above.
    }

}
