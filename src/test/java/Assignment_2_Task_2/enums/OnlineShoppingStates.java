package Assignment_2_Task_2.enums;

public enum OnlineShoppingStates {
    MAINPAGE,
    ACCOUNT,
    CART,
    CHECKOUT,
}
