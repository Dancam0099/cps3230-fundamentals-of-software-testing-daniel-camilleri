package Assignment_1_Task_1.currencymanager;

import org.junit.*;
import java.io.*;
public class CurrencyManagerTest {
    CurrencyManager currencyManager;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;


    @Before
    public void setup() throws Exception {
        currencyManager = new CurrencyManager();
        System.setOut(new PrintStream(outContent));
        System.out.flush();
    }


    @Test
    public void getMajorCurrencyRatesTest() throws Exception {
        Assert.assertEquals(6,currencyManager.getMajorCurrencyRates().size());
    }


    @Test
    public void getExchangeRateCorrectTest() throws Exception {
        Assert.assertNotEquals(0,currencyManager.getExchangeRate("USD","EUR").rate);
    }


    @Test
    public void getExchangeRateExceptionThrownTest(){
        try {
            currencyManager.getExchangeRate("TST","TST");
        }catch (Exception e){
            Assert.assertEquals("Unkown currency: TST",e.getMessage());
        }
    }

    @Test
    public void addCurrencyIncorrectCodeLengthTest(){
        try{
            currencyManager.addCurrency("TSTTTT","Test",true);

        }catch (Exception e){
            Assert.assertEquals("A currency code should have 3 characters.",e.getMessage());
        }

    }

    @Test
    public void addCurrencyIncorrectNameLengthTest(){
        try{
            currencyManager.addCurrency("TST","T",true);
        }catch (Exception e){
            Assert.assertEquals("A currency's name should be at least 4 characters long.",e.getMessage());
        }
    }

    @Test
    public void addCurrencyCurrencyAlreadyExistsExceptionTest(){
        try{
            currencyManager.addCurrency("EUR","Euro",true);
        }catch (Exception e){
            Assert.assertEquals("The currency EUR already exists.",e.getMessage());
        }
    }


    @Test
    public void addCurrencySuccessfulTest() throws Exception{
        currencyManager.addCurrency("TST","Test",true);
        Assert.assertEquals("Test",currencyManager.currencyDatabase.getCurrencyByCode("TST").name);
        currencyManager.currencyDatabase.deleteCurrency("TST");
    }

    @Test
    public void deleteCurrencyWithCodeCurrencyNotInDatabaseTest(){
        try {
            currencyManager.deleteCurrencyWithCode("TST");
        }catch (Exception e){
            Assert.assertEquals("Currency does not exist: TST",e.getMessage());
        }
    }

    @Test
    public void deleteCurrencyWithCodeSuccessfulTest() throws Exception{
        currencyManager.addCurrency("TST","Test",true);
        currencyManager.deleteCurrencyWithCode("TST");
        Assert.assertEquals(5,currencyManager.currencyDatabase.getCurrencies().size());

    }


    @Test
    public void mainMainMenuTestTest() throws Exception {
        String data = "0\n"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertEquals("Main Menu",output[1]);
    }

    @Test
    public void mainPrintCurrenciesTest() throws Exception{
        String data = "1\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertEquals("GBP - British Pound\r",output[16]);
    }

    @Test
    public void mainListExchangeRatesTest() throws Exception{
        String data = "2\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertEquals("Major Currency Exchange Rates",output[12]);
    }


    @Test
    public void mainGetExchangeRateSuccessfulTest()throws Exception{
        String data = "3\nEUR\nGBP\nTST\nTest\ny\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        String[] line = output[13].split(" ");
        Assert.assertEquals("EUR",line[6]);
        currencyManager.currencyDatabase.persist();
    }

    @Test
    public void mainGetExchangeRateExceptionTest() throws Exception{
        String data = "3\nTST\nTST\nTST\nTest\ny\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|:");
        Assert.assertEquals(" Unkown currency TST\r",output[16]+output[17]);
        currencyManager.currencyDatabase.persist();
    }


    @Test
    public void mainAddCurrencySuccessfulTest() throws Exception{
        String data = "4\nTST\nTest\ny\n1\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertEquals("TST - Test\r",output[32]);
        currencyManager.currencyDatabase.persist();
    }

    @Test
    public void mainAddCurrencyWrongMajorMinorSetterTest() throws Exception{
        String data = "4\nTST\nTest\ng\ny\n1\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertEquals("TST - Test\r",output[33]);
        currencyManager.currencyDatabase.persist();
    }

    @Test
    public void mainAddCurrencyAlreadyExistsTest() throws Exception{
        String data = "4\nEUR\nEuro\ny\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        System.setErr(new PrintStream(outContent));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertEquals("The currency EUR already exists.\r",output[13]);
    }


    @Test
    public void mainRemoveCurrencySuccessfulTest() throws Exception{
        String data = "5\nEUR\n1\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertNotEquals("EUR - EURO",output[27]);
        currencyManager.currencyDatabase.persist();
    }

    @Test
    public void mainRemoveCurrencyException() throws Exception{
        String data = "5\nTST\n0"; // input stream
        System.setIn(new ByteArrayInputStream(data.getBytes()));
        System.setErr(new PrintStream(outContent));
        CurrencyManager.main(null);
        String[] output = outContent.toString().split("\n|\n\n");
        Assert.assertNotEquals("Enter the currency code: Currency does not exist: TST\n",output[12]);
    }



    @After
    public void cleanup(){
        currencyManager = null;
        System.setOut(originalOut);
        System.setErr(originalErr);
        System.out.flush();
    }




}
