package Assignment_1_Task_1.currencymanager.currencies;
import org.junit.*;


public class CurrencyTest {
    private Currency currency;


    @Before
    public void setUp()
    {
        currency = new Currency("TST","Test",false);
    }



    @Test
    public void currencyStaticCodeTest() throws Exception {
        currency = Currency.fromString("TST,test,yes");
        Assert.assertEquals("TST",currency.code);
    }

    @Test
    public void currencyStaticNameTest() throws Exception {
        currency = Currency.fromString("TST,test,yes");
        Assert.assertEquals("test",currency.name);
    }

    @Test
    public void currencyStaticMajorTest() throws Exception{
        currency = Currency.fromString("TST,test,yes");
        Assert.assertTrue(currency.major);
    }

    @Test(expected=NullPointerException.class)
    public void currencyStaticExceptionTest() throws Exception {
        currency = Currency.fromString(null);
    }

    @Test
    public void currencyToStringTest(){
        Assert.assertEquals("TST - Test",currency.toString());
    }

    @After
    public void cleanUp()
    {
        currency = null;
    }
}
