package Assignment_1_Task_1.currencymanager.currencies;

import org.junit.*;
import java.io.BufferedWriter;
import java.io.FileWriter;


public class CurrencyDatabaseTest {

    CurrencyDatabase currencyDatabase;

    @Before
    public void setup(){
        try {
            currencyDatabase = new CurrencyDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void currencyExistsTrueTest() throws Exception {

        Assert.assertTrue(currencyDatabase.currencyExists("EUR"));
    }


    @Test
    public void currencyExistsFalseTest(){
        Assert.assertFalse(currencyDatabase.currencyExists("CBD"));
    }

    @Test
    public void getCurrenciesListSizeReturnTest(){
        Assert.assertNotEquals(0,currencyDatabase.getCurrencies().size());
    }

    @Test
    public void getMajorCurrenciesListSizeReturnTest(){
        Assert.assertNotEquals(0,currencyDatabase.getMajorCurrencies().size());
    }

    @Test
    public void getCurrencyByCodeAvailableCurrencyTest(){
        Assert.assertEquals("EUR",currencyDatabase.getCurrencyByCode("EUR").code);
    }

    @Test
    public void getCurrencyByCodeNotAvailableCurrencyTest(){
        Assert.assertNull(currencyDatabase.getCurrencyByCode("TST"));
    }

    @Test
    public void addCurrencySuccessfulTest() throws Exception {
        Currency currency = new Currency("TST","Test",true);
        currencyDatabase.addCurrency(currency);
        Assert.assertEquals("TST",currencyDatabase.getCurrencyByCode("TST").code);
        currencyDatabase.deleteCurrency("TST");
    }




    @Test
    public void deleteCurrencySuccessfulTest() throws Exception{
        Currency currency = new Currency("TST","Test",true);
        currencyDatabase.addCurrency(currency);
        currencyDatabase.deleteCurrency("TST");
        Assert.assertFalse(currencyDatabase.currencyExists("TST"));
    }


    @Test (expected = Exception.class)
    public void getExchangeRateSourceCurrencyNullTest() throws Exception {
        Assert.assertNull(currencyDatabase.getExchangeRate(null,"EUR"));
    }


    @Test (expected = Exception.class)
    public void getExchangeRateDestinationCurrencyNullTest() throws Exception {
        Assert.assertNull(currencyDatabase.getExchangeRate("USD",null));
    }

    @Test
    public void getExchangeCurrencyNotInDatabaseTest() throws Exception{
        Currency currency = new Currency("TST","Test",true);
        currencyDatabase.addCurrency(currency);
        ExchangeRate exchangeRateSource = currencyDatabase.getExchangeRate("USD","EUR");
        ExchangeRate exchangeRateDestination = currencyDatabase.exchangeRates.get("USDEUR");
        Assert.assertEquals(exchangeRateSource.destinationCurrency,exchangeRateDestination.destinationCurrency);
        currencyDatabase.deleteCurrency("TST");
    }


    @Test
    public void getExchangeCurrencyLongerThanFiveMinutesTest() throws Exception {
        Currency currency = new Currency("TST","Test",true);
        currencyDatabase.addCurrency(currency);
        ExchangeRate exchangeRateSourceOld = currencyDatabase.getExchangeRate("USD","EUR");
        currencyDatabase.exchangeRates.get("USDEUR").timeLastChecked = 0;
        ExchangeRate exchangeRateSourceNew = currencyDatabase.getExchangeRate("USD","EUR");
        System.out.println();

        Assert.assertNotEquals(exchangeRateSourceNew.rate,exchangeRateSourceOld.rate);
        currencyDatabase.deleteCurrency("TST");
        //
    }

    @Test
    public void initFirstLineIncorrect() throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(currencyDatabase.currenciesFile));
        writer.write("test\n");
        writer.flush();
        writer.close();
        try{
            currencyDatabase.init();
        }catch (Exception e){
            Assert.assertEquals("Parsing error when reading currencies file.",e.getMessage());
        }
        currencyDatabase.persist();

    }

    @Test
    public void initMoreThanTwoCommasTest() throws Exception{
        BufferedWriter writer = new BufferedWriter(new FileWriter(currencyDatabase.currenciesFile));
        writer.write("code,name,major\n");
        writer.write(",,,,\n");
        writer.flush();
        writer.close();
        try{
            currencyDatabase.init();
        }catch (Exception e){
            Assert.assertEquals("Parsing error: expected two commas in line ,,,,",e.getMessage());
        }
        currencyDatabase.persist();

    }


    @Test
    public void initTooLongCurrencyCodeTest() throws Exception {
        Currency currency = new Currency("TSTTT","Test",true);

        try{
            currencyDatabase.addCurrency(currency);
            currencyDatabase.init();
        }catch (Exception e){
            Assert.assertEquals("Invalid currency code detected: TSTTT",e.getMessage());

        }
        currencyDatabase.deleteCurrency("TSTTT");
    }

    @After
    public void cleanup(){
        currencyDatabase = null;
    }


    /* COULD NOT TEST SINCE A CHECK FOR NULL WAS NOT PRESENT

    @Test
    public void persistExceptionTest() throws Exception {
    currencyDatabase.currencies = null;
    currencyDatabase.persist();

    }
    @Test(expected = NullPointerException.class)
    public void addCurrencyExceptionTest() throws Exception {
        currencyDatabase.addCurrency(null);
    }
    */



}
