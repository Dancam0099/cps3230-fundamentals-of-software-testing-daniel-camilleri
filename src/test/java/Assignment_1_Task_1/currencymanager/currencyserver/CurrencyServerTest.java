package Assignment_1_Task_1.currencymanager.currencyserver;
import org.junit.*;
import org.mockito.Matchers;
import static org.mockito.Mockito.*;
import java.util.Random;

public class CurrencyServerTest {

    CurrencyServer currencyServer;


    @Before
    public void setup(){
        currencyServer = mock(CurrencyServer.class);
    }
    //CAN'T INJECT OBJECT

    /*
        @Test
        public void getExchangeRateTest(){
        Random random = new Random();
        when(currencyServer.getExchangeRate(Matchers.anyString(),Matchers.anyString())).thenReturn(random.nextDouble()*1.5);
    }
     */
    @After
    public void cleanup(){
        currencyServer = null;
    }


}
