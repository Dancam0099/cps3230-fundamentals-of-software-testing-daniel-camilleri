package Assignment_1_Task_1.currencymanager.currencyserver;
import org.junit.*;
public class DefaultCurrencyServerTest {
    CurrencyServer defaultCurrencyServer;

    @Before
    public void setup(){
        defaultCurrencyServer = new DefaultCurrencyServer();

    }

    @Test
    public void getExcangeTest(){
        Assert.assertNotEquals(0,defaultCurrencyServer.getExchangeRate("TDT", "TST"), 0.0);
    }



    @After
    public void cleanup(){
        defaultCurrencyServer = null;
    }


}
